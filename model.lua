-- LUALOCALS < ---------------------------------------------------------
local math, os
    = math, os
local math_exp, math_log, math_random, math_randomseed, os_time
    = math.exp, math.log, math.random, math.randomseed, os.time
-- LUALOCALS > ---------------------------------------------------------

math_randomseed(os_time())

local virus_growth = 1.5
local virus_limit = 1000
local immune_respond = 0.26
local immune_decay = 0.95

local viral = 10
local immune = 0

for _ = 1, 100000 do
	immune = immune * immune_decay + math_exp(viral * immune_respond) - 1
	if immune < 0 then immune = 0 end
	local grow = 1 + (virus_growth - 1) * (1 - (viral / virus_limit))
	* (math_random() * 0.5 + 0.75)
	viral = viral * grow - math_log(1 + immune)
	if viral < 0 then viral = 0 end
	print(viral .. " " .. immune)
end
