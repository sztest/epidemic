-- LUALOCALS < ---------------------------------------------------------
local error, minetest, pairs, rawset, setmetatable
    = error, minetest, pairs, rawset, setmetatable
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

api.aerosol_node = modname .. ":aerosol"
minetest.register_node(api.aerosol_node, {
		drawtype = "airlike",
		pointable = false,
		walkable = false,
		buildable_to = true,
		floodable = true,
		air_equivalent = true,
		paramtype = "light",
		sunlight_propagates = true
	})

function api.get_meta_node(pos, name)
	local metakey = modname .. "_" .. name
	local s = minetest.get_meta(pos):get_string(metakey) or ""
	s = s and s ~= "" and minetest.deserialize(s) or {}
	setmetatable(s, {
			__index = {
				save = function()
					if pairs(s)(s) then
						return minetest.get_meta(pos)
						:set_string(metakey,
							minetest.serialize(s))
					end
					return minetest.get_meta(pos)
					:set_string(metakey, "")
				end
			},
			__newindex = function(t, k, v)
				if k == "save" then return error("write-protected") end
				return rawset(t, k, v)
			end
		})
	return s
end

function api.set_aerosol(pos, metaname)
	local node = minetest.get_node(pos)
	if node.name ~= api.aerosol_node then
		local def = minetest.registered_nodes[node.name]
		if not (def and def.buildable_to and def.air_equivalent) then return end
		minetest.swap_node(pos, {name = api.aerosol_node})
	end
	return api.get_meta_node(pos, metaname)
end

minetest.register_abm({
		nodenames = {api.aerosol_node},
		interval = 1,
		chance = 1,
		action = function(pos)
			local now = minetest.get_gametime()
			local timers = api.get_meta_node(pos, "_timers")
			for _, def in pairs(api.registered) do
				if def.on_tick_node then
					local nextrun = timers[def.name] or now
					if nextrun <= now then
						local meta = api.get_meta_node(pos, def.name)
						repeat
							nextrun = nextrun + def.interval
							def.on_tick_node(pos, meta)
						until nextrun > now
						meta.save()
						timers[def.name] = nextrun
					end
				end
			end
			timers.save()
		end
	})
