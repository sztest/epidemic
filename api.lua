-- LUALOCALS < ---------------------------------------------------------
local minetest, rawset
    = minetest, rawset
-- LUALOCALS > ---------------------------------------------------------

--[[--------------------------------------------------------------------

-- N.B. infection of arbitrary entities/mobs is NOT currently supported,
-- as it would require hooking into the step functions and static data
-- load/save, which is out of scope for this API for now. Handling,
-- including debug display would need to be done by modders.

-- register a new type of plague
api.register({
		name = "name", -- used everywhere as an identifier
		interval = 1, -- seconds between ticks
		on_tick_player = function(player, meta)
			-- runs against each player each tick, whether player
			-- has the illness or not; detect new exposure, evolve
			-- illness, apply effects
		end,
		on_tick_node = function(pos, meta)
			-- runs against each virus-cloud node each tick, if
			-- it has any definition for this illness;
		end,
		on_touch = function(player, pointed_thing, playermeta, nodemeta, is_punch)
			-- when a player touches a surface via punch or right_click;
			-- may not work for all right-click cases if tool or node
			-- defines an override
		end,
		on_debug = function(meta, thing)
			-- get debugging info for this plague; thing will be
			-- {pos={}} or {obj=userdata}; return false to display
			-- nothing, nil for mere presence
			return "text"
		end
	})

-- create an aerosolized virus node at position for given plague name (can
-- coexist with other plagues) if there is air at that node; return metadata
-- table that can be populated and .save()d
local meta = api.set_aerosol(pos, "name")

--]]--------------------------------------------------------------------

local modname = minetest.get_current_modname()
local api = {}
rawset(_G, modname, api)
