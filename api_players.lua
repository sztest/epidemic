-- LUALOCALS < ---------------------------------------------------------
local error, minetest, pairs, rawset, setmetatable
    = error, minetest, pairs, rawset, setmetatable
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local pmetacache = {}
function api.get_meta_player(player, name)
	local pname = player:get_player_name()
	local cached = pmetacache[pname .. "\a" .. name]
	if cached then return cached end
	local metakey = modname .. "_" .. name
	local s = player:get_meta():get_string(metakey) or ""
	s = s and s ~= "" and minetest.deserialize(s) or {}
	setmetatable(s, {
			__index = {
				save = function()
					player = minetest.get_player_by_name(pname)
					if not player then return end
					if pairs(s)(s) ~= nil then
						return player:get_meta():set_string(metakey,
							minetest.serialize(s))
					end
					return player:get_meta():set_string(metakey, "")
				end
			},
			__newindex = function(t, k, v)
				if k == "save" then return error("write-protected") end
				return rawset(t, k, v)
			end
		})
	pmetacache[pname] = s
	return s
end
minetest.register_on_leaveplayer(function(player)
		pmetacache[player:get_player_name()] = nil
	end)

local prevstep = 0
minetest.register_globalstep(function()
		local now = minetest.get_gametime()
		if now == prevstep then return end
		prevstep = now

		local any
		local ticking = {}
		for k, v in pairs(api.registered) do
			if v.on_tick_player then
				ticking[k] = v
				any = true
			end
		end
		if not any then return end

		for _, player in pairs(minetest.get_connected_players()) do
			local timers = api.get_meta_player(player, "_timers")
			for _, def in pairs(ticking) do
				local nextrun = timers[def.name] or now
				if nextrun <= now then
					local meta = api.get_meta_player(player, def.name)
					repeat
						nextrun = nextrun + def.interval
						def.on_tick_player(player, meta)
					until nextrun > now
					meta.save()
					timers[def.name] = nextrun
				end
			end
			timers.save()
		end
	end)
