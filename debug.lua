-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, table, tostring, type, vector
    = minetest, pairs, table, tostring, type, vector
local table_concat
    = table.concat
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local privname = modname .. "_debug"
minetest.register_privilege(privname, {
		description = "see all nearby plague info",
		give_to_admin = false,
		give_to_singleplayer = false
	})

local hudsbypname = {}
minetest.register_on_leaveplayer(function(player)
		hudsbypname[player:get_player_name()] = nil
	end)

local function sethud(player, huds, pos, key, getmeta, thing)
	local text = {}
	for k, v in pairs(api.registered) do
		local meta = getmeta(k)
		if pairs(meta)(meta) ~= nil then
			local x = v.on_debug and v.on_debug(meta, thing)
			if x == nil then x = true end
			if type(x) == "table" then x = minetest.write_json(x) end
			text[#text + 1] = k .. ": " .. tostring(x)
		end
	end
	text = table_concat(text, "\n")

	local old = huds[key]
	if old then
		if text == "" then return end
		old.keep = true
		if text ~= old.text then
			player:hud_change(old.id, "name", text)
			old.text = text
		end
		if not vector.equals(pos, old.pos) then
			player:hud_change(old.id, "world_pos", pos)
			old.pos = pos
		end
		return
	end
	huds[key] = {
		keep = true,
		text = text,
		pos = pos,
		id = player:hud_add({
				hud_elem_type = "waypoint",
				world_pos = pos,
				name = text,
				text = " " .. key,
				number = 0xC04040,
				quick = true
			})
	}
end

local function updateplayer(player)
	local pname = player:get_player_name()
	local huds = hudsbypname[pname]
	if not minetest.check_player_privs(player, privname) then
		if not huds then return end
		for _, v in pairs(huds) do
			player:hud_remove(v.id)
		end
		hudsbypname[pname] = nil
		return
	end
	if not huds then
		huds = {}
		hudsbypname[pname] = huds
	end
	for _, v in pairs(huds) do v.keep = nil end

	local pos = player:get_pos()
	for _, npos in pairs(minetest.find_nodes_in_area(
			{x = pos.x - 5, y = pos.y - 5, z = pos.z - 5},
			{x = pos.x + 5, y = pos.y + 5, z = pos.z + 5},
			api.aerosol_node)) do
		sethud(player, huds, npos, "n:" .. minetest.pos_to_string(npos),
			function(name)
				return api.get_meta_node(npos, name)
			end,
			{pos = npos})
	end
	for _, peer in pairs(minetest.get_connected_players()) do
		local ppos = vector.subtract(peer:get_pos(),
			vector.multiply(peer:get_look_dir(), 0.1))
		ppos.y = ppos.y + peer:get_properties().eye_height
		if vector.distance(ppos, pos) < 7 then
			sethud(player, huds, ppos, "p:" .. peer:get_player_name(),
				function(name)
					return api.get_meta_player(peer, name)
				end,
				{obj = peer})
		end
	end

	for k, v in pairs(huds) do
		if not v.keep then
			player:hud_remove(v.id)
			huds[k] = nil
		end
	end
end

local function tick()
	minetest.after(1, tick)
	for _, player in pairs(minetest.get_connected_players()) do
		updateplayer(player)
	end
end
minetest.after(1, tick)
