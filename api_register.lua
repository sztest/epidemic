-- LUALOCALS < ---------------------------------------------------------
local error, minetest
    = error, minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local unnamed = {}
local function nameassign(name)
	if name then return name end
	local n = minetest.get_current_modname()
	local id = (unnamed[n] or 0) + 1
	unnamed[n] = id
	return n .. "_" .. id
end

api.registered = {}
function api.register(def)
	if def == api then
		return error("use "
			.. modname .. ".register(), not "
			.. modname .. ":register()")
	end
	def.name = nameassign(def.name)
	def.interval = def.interval or 1
	api.registered[def.name] = def
end
