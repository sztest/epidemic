-- LUALOCALS < ---------------------------------------------------------
local math, minetest
    = math, minetest
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local reservoir = modname .. ":reservoir"
minetest.register_node(reservoir, {
		drawtype = "airlike",
		pointable = false,
		walkable = false,
		buildable_to = true,
		air_equivalent = true,
		paramtype = "light",
		sunlight_propagates = true,
		floodable = true
	})

minetest.register_abm({
		nodenames = {reservoir},
		interval = 1,
		chance = 1,
		action = function(pos)
			minetest.add_particle({
					pos = pos,
					exptime = 1,
					glow = -15,
					texture = '[combine:1x1^[noalpha^[colorize:#ffffff:255'
				})
		end
	})

for d = 1, 11 do
	minetest.register_ore({
			name = reservoir .. ":" .. d,
			y_max = (2 ^ (d - 1) - 1) * -16,
			y_min = (2 ^ d - 1) * -16,
			ore_type = "blob",
			ore = reservoir,
			wherein = "air",
			clust_size = 5,
			clust_scarcity = 2 ^ 17 / 1.5 ^ d,
			random_factor = 0,
			noise_params = {
				offset = 0,
				scale = 3,
				spread = {x = 10, y = 25, z = 10},
				seed = 34654,
				octaves = 3,
				persist = 0.5,
				flags = "eased",
			},
			noise_threshold = 1.2
		})
end

api.register({
		name = "test",
		interval = 1,
		on_tick_player = function(_, meta)
			meta.rand = (meta.rand or 0) + math_random()
			minetest.log(meta.rand)
		end,
		on_tick_node = function(_, meta)
			meta.rand = (meta.rand or 0) + math_random()
		end,
		on_debug = function(meta)
			return meta.rand
		end
	})
